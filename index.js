const express = require("express");
const socketIo = require("socket.io");
const app = express();

const server = app.listen(4000, () => {
  console.log("connected");
});
const io = socketIo(server);
const users = {};

io.on("connection", (socket) => {
  socket.on("connected", (user) => {
    users[socket.id] = user;

    io.emit("users", users);
  });

  socket.on("message", (message) => {
    io.emit("message", message);
  });

  socket.on("disconnect", () => {
    delete users[socket.id];
    console.log("disconnected");
    console.log(users);
    io.emit("users", users);
  });
});
